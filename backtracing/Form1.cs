﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace backtracing
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Tube tube1 = new Tube();
        Tube tube2 = new Tube();
        Tube tube3 = new Tube();
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                tube1.scale = Convert.ToInt32(textBox1.Text);
                tube2.scale = Convert.ToInt32(textBox2.Text);
            }
            catch
            {
                MessageBox.Show("Введены неверные данные", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
            }
            tube1.current = 100;
            tube2.current = 0;
            tube3.scale = 100;
            tube3.current = 0;
            Solver solver = new Solver(tube1, tube2, tube3, progressBar1,progressBar2,progressBar3,p1,p2,p3);
            if (solver.Solve())
            {
                MessageBox.Show("Можно получить 1 мл в третьей пробирке", "Ответ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Нельзя получить 1 мл в третьей пробирке", "Ответ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
    }
}
