﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace backtracing
{
    public class Solver
    {
        Tube tube1, tube2, tube3;
        ProgressBar pb1, pb2, pb3;
        Label p1, p2, p3;
        struct State
        {
            public int current1;
            public int current2;
            public int current3;
        }

        List<State> Solution = new List<State>();
        public Solver(Tube t1, Tube t2, Tube t3, ProgressBar ppb1, ProgressBar ppb2, ProgressBar ppb3, Label pp1, Label pp2, Label pp3)
        {
            tube1 = t1;
            tube2 = t2;
            tube3 = t3;
            pb1 = ppb1;
            pb2 = ppb2;
            pb3 = ppb3;
            p1 = pp1;
            p2 = pp2;
            p3 = pp3;
        }

        private void Delay(int timeDelay)
        {
            int i = 0;
            var delayTimer = new System.Timers.Timer();
            delayTimer.Interval = timeDelay;
            delayTimer.AutoReset = false;
            delayTimer.Elapsed += (s, args) => i = 1;
            delayTimer.Start();
            while (i == 0) { };
        }

        public void Move(ref Tube t1, ref Tube t2)
        {
            int del = t1.current % t1.scale;
            if (del == 0)
            {
                t1.current -= t1.scale;
                t2.current += t1.scale;
            }
            else
            {
                t1.current -= del;
                t2.current += del;
            }
        }

        public bool Solve()
        {
            if (tube1.current + tube2.current == 99)
            {
                return true;
            }
            State state = new State();
            state.current1 = tube1.current;
            state.current2 = tube2.current;
            state.current3 = tube3.current;
            while (!Solution.Contains(state) && tube1.current >= 0 && tube2.current >= 0 && tube3.current >= 0 && tube1.current <= 100 && tube2.current <= 100 && tube3.current <= 100)
            {
                p1.Text = tube1.current.ToString();
                p1.Refresh();
                p2.Text = tube2.current.ToString();
                p2.Refresh();
                p3.Text = tube3.current.ToString();
                p3.Refresh();
                pb1.Value = tube1.current;
                pb2.Value = tube2.current;
                pb3.Value = tube3.current;
                Delay(1000);
                Solution.Add(state);
                Move(ref tube1, ref tube2);
                if (!Solve())
                {
                    Move(ref tube2, ref tube3);
                    if (!Solve())
                    {
                        Move(ref tube3, ref tube1);

                        if (!Solve())
                        {
                            Move(ref tube1, ref tube3);

                            if (!Solve())
                            {
                                Move(ref tube2, ref tube1);

                                if (!Solve())
                                {
                                    Move(ref tube3, ref tube2);
                                    if (Solve())
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                tube1.current = Solution[Solution.Count - 1].current1;
                tube2.current = Solution[Solution.Count - 1].current2;
                tube3.current = Solution[Solution.Count - 1].current3;
            }
            return false;
        }
    }
}
